# React Native

REACT NATIVE

## Installation




```bash
npm install --global expo-cli 
expo init my-project
```

## Usage

```React Native
import React from 'react-native'

```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)